package com.target.git.features.display_data;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.widget.Toast;
import com.target.git.R;
import com.target.git.databinding.ActivityDataBinding;

public class DisplayDataActivity extends AppCompatActivity implements DisplayDataNavigator {

    private DataRecyclerViewAdapter mAdapter = null;
    private DataViewModel mViewModel;
    private ActivityDataBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new DataViewModel(this.getApplication());

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_data);
        mBinding.setViewModel(mViewModel);
        mViewModel.setDisplayDataNavigator(this);
        subscribeToLiveData();

        getData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getData() {
        mViewModel.getTrendingRepos();
    }

    private void subscribeToLiveData() {
        mViewModel.getRepoData().observe(this,
                dataList -> {
                    if (mAdapter == null) {
                        mAdapter = new DataRecyclerViewAdapter(DisplayDataActivity.this, dataList);
                        mBinding.recyclerView.setAdapter(mAdapter);
                        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());

                    } else {
                        mAdapter.setDataList(dataList);
                        mAdapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    public void handleServerError(Throwable t) {
        String message = getString(R.string.server_error);
        if (t != null && !TextUtils.isEmpty(t.getMessage())) {
            message = t.getMessage();
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleNoInternetError() {
        String message = getString(R.string.data_error);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}
