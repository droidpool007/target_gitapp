package com.target.git.features.display_data;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.target.git.BR;
import com.target.git.Constants;
import com.target.git.R;
import com.target.git.features.display_details.DetailsActivity;
import com.target.git.models.TrendingRepoModel;

import java.util.ArrayList;
import java.util.List;

public class DataRecyclerViewAdapter extends RecyclerView.Adapter<DataRecyclerViewAdapter.DataViewHolder> {

    private ArrayList<TrendingRepoModel> mDataList = null;
    private Activity mActivity = null;

    public DataRecyclerViewAdapter(Activity activity, List<TrendingRepoModel> list) {
        mActivity = activity;
        mDataList = (ArrayList) list;
    }

    public void setDataList(List<TrendingRepoModel> list) {
        mDataList = (ArrayList) list;
    }

    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_data_row, parent, false);
        return new DataViewHolder(binding);
    }

    public void onBindViewHolder(DataViewHolder holder, int position) {
        if (mDataList != null) {
            TrendingRepoModel model = mDataList.get(position);
            if (model != null) {
                DataItemViewModel viewModel = new DataItemViewModel(model);
                holder.bind(viewModel);
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mActivity, DetailsActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.putExtra(Constants.BUNDLE_DEATILS_MODEL, model);
                        mActivity.startActivity(i);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;
        private View view;

        public DataViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.view = binding.getRoot();
        }

        public void bind(DataItemViewModel viewModel) {
            binding.setVariable(BR.viewModel, viewModel);
            binding.executePendingBindings();
        }
    }
}
