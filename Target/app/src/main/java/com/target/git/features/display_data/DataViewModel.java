package com.target.git.features.display_data;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import com.target.git.Utils;
import com.target.git.models.TrendingRepoModel;
import com.target.git.network.ApiClient;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class DataViewModel extends AndroidViewModel {

    private DisplayDataNavigator mDisplayDataNavigator;
    private ObservableBoolean mIsLoading = new ObservableBoolean(true);
    private MutableLiveData<List<TrendingRepoModel>> mDataLiveData = new MutableLiveData<>();

    public DataViewModel(@NonNull Application application) {
        super(application);
    }

    public void setDisplayDataNavigator(DisplayDataNavigator navigator) {
        this.mDisplayDataNavigator = navigator;
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.mIsLoading.set(isLoading);
    }

    public void getTrendingRepos() {
        if (Utils.isInternetAvailable(getApplication().getBaseContext())) {
            setIsLoading(true);

            CompositeDisposable compositeDisposable = new CompositeDisposable();
            compositeDisposable.add(ApiClient.getApiClient().getTrendingRepos()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(modelList -> {
                        if (modelList != null) {
                            setRepoData(modelList);
                        }
                        setIsLoading(false);
                    }, throwable -> {
                        setIsLoading(false);
                        mDisplayDataNavigator.handleServerError(throwable);
                    }));
        } else {
            setIsLoading(false);
            mDisplayDataNavigator.handleNoInternetError();
        }
    }

    public MutableLiveData<List<TrendingRepoModel>> getRepoData() {
        return mDataLiveData;
    }

    public void setRepoData(List<TrendingRepoModel> data) {
        this.mDataLiveData.setValue(data);
    }
}
