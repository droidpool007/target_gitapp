package com.target.git.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class RepoModel implements Serializable {
    @Expose
    String name;
    @Expose
    String url;
    @Expose
    String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
