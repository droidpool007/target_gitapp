package com.target.git.features.display_data;

interface DisplayDataNavigator {

    void handleServerError(Throwable t);

    void handleNoInternetError();
}
