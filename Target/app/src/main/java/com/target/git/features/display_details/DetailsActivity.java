package com.target.git.features.display_details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.target.git.R;
import com.target.git.databinding.ActivityDetailsBinding;

public class DetailsActivity extends AppCompatActivity implements DetailsNavigator {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DetailsViewModel viewModel = new DetailsViewModel();

        ActivityDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        binding.setViewModel(viewModel);
        viewModel.setNavigator(this);

        viewModel.displayDetails(getIntent());
    }

    @Override
    public void handleError() {
        String message = getString(R.string.data_error);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
