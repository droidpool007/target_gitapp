package com.target.git.network;

import com.target.git.models.TrendingRepoModel;
import io.reactivex.Observable;
import retrofit2.http.GET;

import java.util.List;

public interface ServerApi {
    String BASE_URL = "https://github-trending-api.now.sh/";
    String GET_GIT_TRENDING_REPOS = "developers?language=java&amp;since=weekly";

    @GET(GET_GIT_TRENDING_REPOS)
    Observable<List<TrendingRepoModel>> getTrendingRepos();
}
