package com.target.git.features.display_data;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import com.target.git.models.TrendingRepoModel;

public class DataItemViewModel extends ViewModel {

    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mRepoName = new ObservableField<>();
    private ObservableField<String> mUsername = new ObservableField<>();
    private ObservableField<String> mAvatar = new ObservableField<>();

    public DataItemViewModel(TrendingRepoModel model) {
        setName(model.getName());
        setUsername(model.getUsername());
        setAvatar(model.getAvatar());
        if (model.getRepoModel() != null) {
            setRepoName(model.getRepoModel().getName());
        }
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName.set(name);
    }

    public ObservableField<String> getRepoName() {
        return mRepoName;
    }

    public void setRepoName(String repoName) {
        this.mRepoName.set(repoName);
    }

    public ObservableField<String> getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername.set(username);
    }

    public ObservableField<String> getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        this.mAvatar.set(avatar);
    }
}

