package com.target.git.features.display_details;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.databinding.ObservableField;
import com.target.git.Constants;
import com.target.git.models.TrendingRepoModel;

public class DetailsViewModel extends ViewModel {

    private DetailsNavigator mNavigator;
    public ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mUserName = new ObservableField<>();
    private ObservableField<String> mAvatar = new ObservableField<>();
    private ObservableField<String> mRepoName = new ObservableField<>();
    private ObservableField<String> mDescription = new ObservableField<>();
    private ObservableField<String> mUrl = new ObservableField<>();
    private ObservableField<String> mRepoUrl = new ObservableField<>();

    public void setNavigator(DetailsNavigator navigator) {
        this.mNavigator = navigator;
    }

    public void displayDetails(Intent i) {
        TrendingRepoModel model = ((TrendingRepoModel) i.getSerializableExtra(Constants.BUNDLE_DEATILS_MODEL));
        if (model == null) {
            mNavigator.handleError();
        } else {
            setName(model.getName());
            setUsername(model.getUsername());
            setAvatar(model.getAvatar());
            setUrl(model.getUrl());
            if (model.getRepoModel() != null) {
                setRepoName(model.getRepoModel().getName());
                setDescription(model.getRepoModel().getDescription());
                setRepoUrl(model.getRepoModel().getUrl());
            }
        }
    }

    public ObservableField<String> getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName.set(name);
    }

    public ObservableField<String> getUsername() {
        return mUserName;
    }

    public void setUsername(String username) {
        this.mUserName.set(username);
    }

    public ObservableField<String> getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        this.mAvatar.set(avatar);
    }

    public ObservableField<String> getRepoName() {
        return mRepoName;
    }

    public void setRepoName(String repoName) {
        this.mRepoName.set(repoName);
    }

    public ObservableField<String> getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription.set(description);
    }

    public ObservableField<String> getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl.set(url);
    }

    public ObservableField<String> getRepoUrl() {
        return mRepoUrl;
    }

    public void setRepoUrl(String repoUrl) {
        this.mRepoUrl.set(repoUrl);
    }
}
