package com.target.git;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import com.target.git.features.display_data.DisplayDataActivity;
import com.target.git.models.RepoModel;
import com.target.git.models.TrendingRepoModel;
import com.target.git.network.ServerApi;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import junit.framework.AssertionFailedError;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DataActivityInstrumentationTest {
    @Mock
    ServerApi serverApi;

    @Rule
    public ActivityTestRule<DisplayDataActivity> mWeatherActivityTestRule = new ActivityTestRule<>(DisplayDataActivity.class);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchTrendingRepos() {
        Mockito.when(serverApi.getTrendingRepos()).thenReturn(Observable.just(getTrendingRepoList()));

        TestObserver<List<TrendingRepoModel>> testObserver = serverApi.getTrendingRepos().test();
        testObserver.awaitTerminalEvent();
        testObserver
                .assertNoErrors()
                .assertValue(modelList -> modelList.size() > 0)
                .assertValue(modelList -> modelList.get(0).getName().equalsIgnoreCase("dummy_trending_model"));
    }

    @Test
    public void testRecyclerView() {
        try {
            Espresso.onView(ViewMatchers.withId(R.id.recyclerView)).perform(RecyclerViewActions.scrollToPosition(3));
        } catch (AssertionFailedError e) {
            // View not displayed
        }
    }

    private List<TrendingRepoModel> getTrendingRepoList() {
        List<TrendingRepoModel> modelList = new ArrayList<TrendingRepoModel>(5);
        RepoModel repoModel = new RepoModel();
        repoModel.setName("dummy_repo");
        repoModel.setDescription("dummy_desc");
        repoModel.setUrl("dummy_url");

        TrendingRepoModel trendingRepoModel = new TrendingRepoModel();
        trendingRepoModel.setName("dummy_trending_model");
        trendingRepoModel.setUsername("dummy_username");
        trendingRepoModel.setAvatar("dummy_avatar");
        trendingRepoModel.setRepoModel(repoModel);

        modelList.add(trendingRepoModel);
        modelList.add(trendingRepoModel);
        modelList.add(trendingRepoModel);
        modelList.add(trendingRepoModel);
        modelList.add(trendingRepoModel);

        return modelList;
    }

}
