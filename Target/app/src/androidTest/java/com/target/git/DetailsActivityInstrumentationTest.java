package com.target.git;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import com.target.git.features.display_details.DetailsActivity;
import com.target.git.features.display_details.DetailsViewModel;
import com.target.git.models.RepoModel;
import com.target.git.models.TrendingRepoModel;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DetailsActivityInstrumentationTest {

    @Rule
    public ActivityTestRule<DetailsActivity> mActivityTestRule = new ActivityTestRule<>(DetailsActivity.class);

    @Test
    public void updateUI() {
        DetailsViewModel viewModel = new DetailsViewModel();
        viewModel.setUsername("dummy_username");
        Assert.assertEquals("dummy_username", viewModel.getUsername().get());
    }

    @Test
    public void displayDetails() {
        DetailsViewModel viewModel = new DetailsViewModel();
        viewModel.displayDetails(generateDataIntent());

        Assert.assertEquals("dummy_trending_model", viewModel.getName().get());
    }

    private Intent generateDataIntent() {
        TrendingRepoModel model = generateModel();
        Intent i = new Intent();
        i.putExtra(Constants.BUNDLE_DEATILS_MODEL, model);
        return i;
    }

    private TrendingRepoModel generateModel() {
        RepoModel repoModel = new RepoModel();
        repoModel.setName("dummy_repo");
        repoModel.setDescription("dummy_desc");
        repoModel.setUrl("dummy_url");

        TrendingRepoModel trendingRepoModel = new TrendingRepoModel();
        trendingRepoModel.setName("dummy_trending_model");
        trendingRepoModel.setUsername("dummy_username");
        trendingRepoModel.setAvatar("dummy_avatar");
        trendingRepoModel.setRepoModel(repoModel);

        return trendingRepoModel;
    }

}